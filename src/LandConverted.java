import java.util.Scanner;

public class LandConverted {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// convert the land from acre to square feet
		// 1.get the number of square feet you want to convert
		// 2.do the conversion calculation
		// ---> 50 square feet =----acres
		// ---> 1 acre = 45650 square feet.
		// --->50 square feet /45650 = [x] acres.
		// 3.output the result to screen

		Scanner keyboard = new Scanner(System.in);
		System.out.println("how many square feet?");
		int squareFeet = keyboard.nextInt();
		double acres = squareFeet / 43560;
		// double acres = (double)squareFeet / 43560 ; Explicit conversion
		// double acres = squareFeet / 43560.00 ;
		System.out.println("number of acres " + acres);

	}

}

	

