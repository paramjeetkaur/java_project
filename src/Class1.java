
public class Class1 {
	public static void main(String[] args) {
		int x = 10;
		double y = 15;
		System.out.println(x + y);
		System.out.println(10 + 5);
		System.out.println(10 - y);
		System.out.println(5 / 4); // expected output is 1.25 //called integer division produce integer output
		System.out.println(5.0 / 4.0); // decimal division //if any one is decimal number then outcome is decimal .
		System.out.println(5.0 / 4);
		System.out.println(5 / 4.0);
		System.out.println(4 / 5);
		System.out.println(4 / 5.0);
		// what is the order of math
		// 0.brackets
		// 1. multiply and divide
		// 2.add and subtract

		int k = 7; // int k=7; //output is 7.0 because its double. //use double instead of int.
		int j = 2; // int j=2;
		double m = k / j; // use int also here and then check //calculation is always done first by
							// computer and then its saved to m.
		System.out.println(m);

	}

}
